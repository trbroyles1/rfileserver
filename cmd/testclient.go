package cmd

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/trbroyles1/rfileserver/ioclient"
)

var testClientCmd = &cobra.Command{
	Use:   "testclient",
	Short: "Runs a test client",
	Run:   testClient,
}

var clientConfig *viper.Viper

func init() {
	testClientCmd.Flags().String("server", "localhost:5678", "the ip:port of the server to connect to")
	testClientCmd.Flags().String("client-name", "testClient", "the ip:port of the server to connect to")

	clientConfig = viper.New()
	clientConfig.BindPFlags(testClientCmd.Flags())

	rootCmd.AddCommand(testClientCmd)
}

func testClient(cmd *cobra.Command, args []string) {
	clientName := clientConfig.GetString("client-name")
	serverAddr := clientConfig.GetString("server")

	client, err := ioclient.Connect(clientName, serverAddr)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Enter a file name: ")
	scanner.Scan()
	fileName := scanner.Text()

	f, err := client.Create(fileName)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Enter some content: ")
	scanner.Scan()
	content := scanner.Bytes()

	fmt.Println("writing to server...")
	if _, err := f.Write(content); err != nil {
		log.Fatal(err)
	}
	fmt.Println("data written.")
	if err := f.Close(); err != nil {
		log.Fatal(err)
	}

	fmt.Println("waiting 5s...")
	tmr := time.NewTimer(5 * time.Second)
	<-tmr.C

	fmt.Println("Reading back...")

	f, err = client.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	readBack, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(readBack))
}
