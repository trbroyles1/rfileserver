package cmd

import (
	"net"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	logging "gitlab.com/trbroyles1/golog"
	"google.golang.org/grpc"

	"gitlab.com/trbroyles1/rfileserver/internal"
	thisrpc "gitlab.com/trbroyles1/rfileserver/rpc"
)

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run the server",
	RunE:  run,
}

var serverConfig *viper.Viper

func init() {
	runCmd.Flags().String("base-directory", "repo", "The base directory to use for storing files")
	runCmd.Flags().String("bind-address", ":5678", "the ip:port to bind to")
	runCmd.Flags().String("log-level", "INFO", "Verbosity of logging, one of ERROR|WARN|INFO|DEBUG")
	runCmd.Flags().Duration("file-timeout", 60*time.Second, "How long to hold a file open with no activity from client for that file")
	runCmd.Flags().String("setuid-user", "", "User ID to change to after chrooting into the base-directory of the file server (Required; cannot be 0 (aka root))")

	runCmd.MarkFlagRequired("setuid-user")

	serverConfig = viper.NewWithOptions(
		viper.EnvKeyReplacer(strings.NewReplacer("-", "_")),
	)

	serverConfig.SetEnvPrefix("rfileserver")
	serverConfig.BindPFlags(runCmd.Flags())

	serverConfig.AutomaticEnv()
	serverConfig.AddConfigPath(".")
	serverConfig.AddConfigPath("/etc/rfileserver")
	serverConfig.SetConfigName("rfileserver")
	serverConfig.SetConfigType("toml")
	serverConfig.ReadInConfig()
	serverConfig.WatchConfig()

	rootCmd.AddCommand(runCmd)
}

func run(cmd *cobra.Command, args []string) error {
	logger := logging.NewConsoleLogger(
		logging.WithColorizedOutput(),
	)

	if uid := syscall.Getuid(); uid != 0 {
		logger.Fatal("Must be run as root. Root permissions are immediately dropped after chrooting into base-directory.")
	}

	parsedSetuid, err := strconv.ParseInt(serverConfig.GetString("setuid-user"), 10, 32)
	if err != nil {
		logger.Fatal(err.Error())
	} else if parsedSetuid < 1 {
		logger.Fatal("setuid-user cannot be root")
	}

	switch serverConfig.GetString("log-level") {
	case "ERROR":
		logger.SetOutputLevel(logging.LevelError)
	case "WARN":
		logger.SetOutputLevel(logging.LevelWarn)
	case "INFO":
		logger.SetOutputLevel(logging.LevelInfo)
	case "DEBUG":
		logger.SetOutputLevel(logging.LevelDebug)
	default:
		logger.Fatalf("Specified log level %s is invalid. Valid values are: ERROR|WARN|INFO|DEBUG", serverConfig.GetString("log-level"))
	}

	logger.Initf("chrooting to %s", serverConfig.GetString("base-directory"))
	if err := os.Chdir(serverConfig.GetString("base-directory")); err != nil {
		logger.Fatal(err.Error())
	}

	if err := syscall.Chroot(serverConfig.GetString("base-directory")); err != nil {
		logger.Fatal(err.Error())
	}

	logger.Init("Dropping root permissions")
	if err := syscall.Setuid(int(parsedSetuid)); err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debugf("Binding to %s", serverConfig.GetString("bind-address"))

	endpoint, err := net.Listen("tcp", serverConfig.GetString("bind-address"))
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Initf("Bound to %s", serverConfig.GetString("bind-address"))

	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	thisrpc.RegisterFileStoreServiceServer(grpcServer, internal.NewFileServer(logger, serverConfig))

	go func() {
		if err := grpcServer.Serve(internal.NewConnectionLoggingListener(logger, endpoint)); err != nil {
			logger.Fatal(err.Error())
		}
	}()

	logger.Initf("Server started; use [Ctrl+C] or SIGINT to terminate")

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)

	<-sigint

	logger.Initf("Caught SIGINT; shutting down")
	grpcServer.GracefulStop()

	return nil
}
