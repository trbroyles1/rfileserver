package main

import (
	"gitlab.com/trbroyles1/rfileserver/cmd"
)

func main() {
	cmd.Execute()
}
