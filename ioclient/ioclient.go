package ioclient

import (
	"io"

	"gitlab.com/trbroyles1/rfileserver/internal"
)

type ioClient struct {
	bc *internal.BasicClient
}

func (ioc *ioClient) Create(name string) (io.ReadWriteCloser, error) {
	if err := ioc.bc.CreateFile(name); err != nil {
		return nil, err
	}
	return &ioSingleFileClient{fileName: name, wrappedClient: ioc}, nil
}

func (ioc *ioClient) Open(name string) (io.ReadWriteCloser, error) {
	if err := ioc.bc.OpenFile(name); err != nil {
		return nil, err
	}
	return &ioSingleFileClient{fileName: name, wrappedClient: ioc}, nil
}

func (ioc *ioClient) Close() error {
	return ioc.bc.Close()
}

func Connect(clientName, serverAddr string) (*ioClient, error) {
	bc, err := internal.NewBasicClient(clientName)
	if err != nil {
		return nil, err
	}
	if err = bc.Connect(serverAddr); err != nil {
		return nil, err
	}

	return &ioClient{
		bc: bc,
	}, nil
}

type ioSingleFileClient struct {
	fileName      string
	wrappedClient *ioClient
}

func (fc *ioSingleFileClient) Read(p []byte) (n int, err error) {
	return fc.wrappedClient.bc.ReadFile(fc.fileName, p)
}

func (fc *ioSingleFileClient) Write(p []byte) (n int, err error) {
	return fc.wrappedClient.bc.WriteFile(fc.fileName, p)
}

func (fc *ioSingleFileClient) Close() error {
	return fc.wrappedClient.bc.CloseFile(fc.fileName)
}
