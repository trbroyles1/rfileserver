package internal

//NEED:
//Create / Touch method for files
//Stat method for files

import (
	"context"
	"crypto"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/pem"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/spf13/viper"
	logging "gitlab.com/trbroyles1/golog"
	thisrpc "gitlab.com/trbroyles1/rfileserver/rpc"
)

const CLIENT_PUBKEY_FILE = "clientkey.pub.pem" //what the file name for the client public key is

type fileServerAction string

const (
	actionRead   fileServerAction = "Read"
	actionWrite  fileServerAction = "Write"
	actionOpen   fileServerAction = "Open"
	actionCreate fileServerAction = "Create"
	actionStat   fileServerAction = "Stat"
	actionClose  fileServerAction = "Close"
)

type FileServer struct {
	thisrpc.UnimplementedFileStoreServiceServer                //required by protobuf in order to be forward compatible if the buffer definition changes in the future
	logger                                      logging.Logger //where to log stuff
	config                                      *viper.Viper   //our config
	clients                                     map[string]*clientHandle
}

type clientHandle struct {
	name      string
	mtx       sync.RWMutex
	ioHandles map[string]*ioHandle
}

type ioHandle struct {
	mtx     sync.Mutex
	timeout *time.Timer
	handle  io.ReadWriteCloser
}

func newClientHandle(name string) *clientHandle {
	return &clientHandle{
		name:      name,
		ioHandles: make(map[string]*ioHandle),
	}
}

//NewFileServer makes a new instance of FileServer with the provided loggger and config and returns a pointer to it
func NewFileServer(logger logging.Logger, config *viper.Viper) *FileServer {
	return &FileServer{
		config:  config,
		logger:  logger,
		clients: make(map[string]*clientHandle),
	}
}

func (fs *FileServer) StatFile(ctx context.Context, req *thisrpc.StatFileRequest) (*thisrpc.StatFileResponse, error) {
	if ok, response := fs.validateActionRequest(actionOpen, req.GetReqDetails()); !ok {
		return &thisrpc.StatFileResponse{RespDetails: response}, nil
	}

	fileName := req.GetReqDetails().GetFileName()
	clientName := req.GetReqDetails().GetClientName()

	fs.logger.Debugf("Handling stat request for %s, client %s", fileName, clientName)

	fileLocation := filepath.Join(clientName, fileName)
	stat, err := os.Stat(fileLocation)
	if err != nil {
		fs.logger.Err(err.Error())

		if os.IsNotExist(err) {
			return &thisrpc.StatFileResponse{RespDetails: errNotFound(fileName)}, nil
		} else {
			return &thisrpc.StatFileResponse{RespDetails: errServerError(fileName, err)}, nil
		}
	}

	return &thisrpc.StatFileResponse{
		RespDetails: &thisrpc.FileActionResponse{
			FileName: req.ReqDetails.GetFileName(),
		},
		Size:    stat.Size(),
		ModTime: stat.ModTime().Unix(),
	}, nil
}

func (fs *FileServer) CreateFile(ctx context.Context, req *thisrpc.CreateFileRequest) (*thisrpc.CreateFileResponse, error) {
	resp, err := fs.createOrOpenClientFile(actionCreate, req)
	return resp.(*thisrpc.CreateFileResponse), err
}

func (fs *FileServer) OpenFile(ctx context.Context, req *thisrpc.OpenFileRequest) (*thisrpc.OpenFileResponse, error) {
	resp, err := fs.createOrOpenClientFile(actionOpen, req)
	return resp.(*thisrpc.OpenFileResponse), err
}

//ReadFile handles the gRPC FileStoreService ReadFile operation defined in server.proto
func (fs *FileServer) ReadFromFile(ctx context.Context, req *thisrpc.FileReadRequest) (*thisrpc.FileReadResponse, error) {
	resp, err := fs.readOrWriteClientFile(actionRead, req)
	return resp.(*thisrpc.FileReadResponse), err
}

//WriteFile handles the gRPC FileStoreService WriteFile operation defined in server.proto
func (fs *FileServer) WriteToFile(ctx context.Context, req *thisrpc.FileWriteRequest) (*thisrpc.FileWriteResponse, error) {
	resp, err := fs.readOrWriteClientFile(actionWrite, req)
	return resp.(*thisrpc.FileWriteResponse), err
}

func (fs *FileServer) CloseFile(ctx context.Context, req *thisrpc.CloseFileRequest) (*thisrpc.CloseFileResponse, error) {
	if ok, response := fs.validateActionRequest(actionClose, req.GetReqDetails()); !ok {
		return &thisrpc.CloseFileResponse{RespDetails: response}, nil
	}

	fileName := req.GetReqDetails().GetFileName()
	clientName := req.GetReqDetails().GetClientName()

	fs.logger.Debugf("Handling Close request for %s, client %s", fileName, clientName)

	if client, c_ok := fs.clients[clientName]; c_ok {
		if _, ok := client.ioHandles[fileName]; ok {
			fs.closeClientIOHandle(client, fileName)()
			return &thisrpc.CloseFileResponse{RespDetails: &thisrpc.FileActionResponse{FileName: fileName}}, nil

		} else {
			return &thisrpc.CloseFileResponse{RespDetails: errNotOpen(fileName)}, nil
		}
	} else {
		return &thisrpc.CloseFileResponse{RespDetails: errNotOpen(fileName)}, nil
	}
}

type reqDetailsGetter interface {
	GetReqDetails() *thisrpc.FileActionRequest
}

func (fs *FileServer) createOrOpenClientFile(action fileServerAction, req interface{}) (interface{}, error) {
	if action != actionCreate && action != actionOpen {
		panic("createOrOpenClientFile called with invalid action")
	}

	var rdg reqDetailsGetter = req.(reqDetailsGetter)

	if ok, response := fs.validateActionRequest(action, rdg.GetReqDetails()); !ok {
		switch action {
		case actionCreate:
			return &thisrpc.CreateFileResponse{RespDetails: response}, nil
		case actionOpen:
			return &thisrpc.FileWriteResponse{RespDetails: response}, nil
		}
	}

	fileName := rdg.GetReqDetails().GetFileName()
	clientName := rdg.GetReqDetails().GetClientName()

	fs.logger.Debugf("Handling %s request for %s, client %s", action, fileName, clientName)
	if _, ok := fs.clients[clientName]; !ok {
		fs.clients[clientName] = newClientHandle(clientName)
	}

	if h, ok := fs.clients[clientName].ioHandles[fileName]; ok {
		h.mtx.Lock()
		defer h.mtx.Unlock()
		h.timeout.Reset(fs.config.GetDuration("file-timeout"))

		switch action {
		case actionCreate:
			return &thisrpc.CreateFileResponse{RespDetails: errAlreadyOpen(fileName)}, nil
		case actionOpen:
			return &thisrpc.OpenFileResponse{RespDetails: errAlreadyOpen(fileName)}, nil
		}
	} else {
		client := fs.clients[clientName]
		client.mtx.Lock()
		defer client.mtx.Unlock()

		path := filepath.Join(clientName, fileName)

		var f io.ReadWriteCloser
		var err error
		switch action {
		case actionCreate:
			f, err = os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
		case actionOpen:
			f, err = os.OpenFile(path, os.O_RDWR, 0755)
		}

		if err != nil {
			switch action {
			case actionCreate:
				if os.IsNotExist(err) {
					return &thisrpc.CreateFileResponse{RespDetails: errNotFound(fileName)}, nil
				} else {
					return &thisrpc.CreateFileResponse{RespDetails: errServerError(fileName, err)}, nil
				}

			case actionOpen:
				if os.IsNotExist(err) {
					return &thisrpc.OpenFileResponse{RespDetails: errNotFound(fileName)}, nil
				} else {
					return &thisrpc.OpenFileResponse{RespDetails: errServerError(fileName, err)}, nil
				}
			}
		}

		h := &ioHandle{
			handle: f,
		}
		h.timeout = time.AfterFunc(fs.config.GetDuration("file-timeout"), fs.closeClientIOHandle(client, fileName))

		client.ioHandles[fileName] = h

		switch action {
		case actionCreate:
			return &thisrpc.CreateFileResponse{RespDetails: &thisrpc.FileActionResponse{FileName: fileName}}, nil
		case actionOpen:
			return &thisrpc.OpenFileResponse{RespDetails: &thisrpc.FileActionResponse{FileName: fileName}}, nil
		}
	}

	panic("createOrOpenClientFile: invalid execution path")

}

func (fs *FileServer) readOrWriteClientFile(action fileServerAction, in interface{}) (interface{}, error) {
	if action != actionRead && action != actionWrite {
		panic("readOrWriteClientFile called with invalid action")
	}

	var rdg reqDetailsGetter = in.(reqDetailsGetter)

	if ok, response := fs.validateActionRequest(action, rdg.GetReqDetails()); !ok {
		switch action {
		case actionRead:
			return &thisrpc.FileReadResponse{RespDetails: response}, nil
		case actionWrite:
			return &thisrpc.FileWriteResponse{RespDetails: response}, nil
		}
	}

	fileName := rdg.GetReqDetails().GetFileName()
	clientName := rdg.GetReqDetails().GetClientName()

	fs.logger.Debugf("Handling %s request for %s, client %s", action, fileName, clientName)

	//fileLocation := filepath.Join(clientName, fileName) //where we need to write to
	var handle *ioHandle
	var h_ok bool

	if client, c_ok := fs.clients[clientName]; c_ok {
		client.mtx.RLock()
		defer client.mtx.RUnlock()
		handle, h_ok = client.ioHandles[fileName]
	}

	if !h_ok {
		switch action {
		case actionRead:
			return &thisrpc.FileReadResponse{RespDetails: errNotOpen(fileName)}, nil
		case actionWrite:
			return &thisrpc.FileWriteResponse{RespDetails: errNotOpen(fileName)}, nil
		}
	}

	handle.mtx.Lock()
	defer handle.mtx.Unlock()
	defer handle.timeout.Reset(fs.config.GetDuration("file-timeout"))
	var response interface{}

	switch action {
	case actionRead:
		var req *thisrpc.FileReadRequest = in.(*thisrpc.FileReadRequest)
		fs.logger.Debugf("Reading up to %d bytes for client %s file %s", req.Len, req.GetReqDetails().GetClientName(), req.GetReqDetails().GetFileName())

		r := &thisrpc.FileReadResponse{RespDetails: &thisrpc.FileActionResponse{FileName: req.GetReqDetails().GetFileName()}}
		r.Data = make([]byte, req.GetLen())

		read, err := handle.handle.Read(r.Data)
		fs.logger.Debugf("Read %d bytes for client %s file %s", read, req.GetReqDetails().GetClientName(), req.GetReqDetails().GetFileName())
		r.Len = int32(read)

		if err != nil {
			if err == io.EOF {
				fs.logger.Debugf("EOF for client %s file %s", req.GetReqDetails().GetClientName(), req.GetReqDetails().GetFileName())
				r.RespDetails = errEndOfFile(fileName)
			} else {
				fs.logger.Err(err.Error())
				r.RespDetails = errServerError(fileName, err)
			}
		} else {
			r.RespDetails = &thisrpc.FileActionResponse{FileName: fileName}
		}

		response = r

	case actionWrite:
		var req *thisrpc.FileWriteRequest = in.(*thisrpc.FileWriteRequest)
		fs.logger.Debugf("Writing %d bytes for client %s file %s", len(req.Data), req.GetReqDetails().GetClientName(), req.GetReqDetails().GetFileName())

		r := &thisrpc.FileWriteResponse{RespDetails: &thisrpc.FileActionResponse{FileName: req.GetReqDetails().GetFileName()}}
		written, err := handle.handle.Write(req.GetData())
		r.Len = int32(written)
		if err != nil {
			fs.logger.Err(err.Error())
			r.RespDetails = errServerError(fileName, err)
		}

		response = r
	}

	fs.logger.Infof("%s of %s for %s completed", action, fileName, clientName)
	return response, nil
}

type accessRequester interface {
	GetClientName() string
	GetDateTime() string
	GetClientSignature() []byte
}

func (fs *FileServer) clientIsValid(r accessRequester) bool {
	clientDir := filepath.Join(r.GetClientName())

	if _, err := os.Stat(clientDir); os.IsNotExist(err) {
		fs.logger.Warnf("Client directory for %s does not exist", r.GetClientName())
		return false
	}

	clientPubKeyRawBytes, err := ioutil.ReadFile(filepath.Join(clientDir, CLIENT_PUBKEY_FILE))
	if err != nil {
		fs.logger.Warn(err.Error())
		return false
	}

	clientPubKeyPem, _ := pem.Decode(clientPubKeyRawBytes)

	clientPubKey, err := x509.ParsePKIXPublicKey(clientPubKeyPem.Bytes)
	if err != nil {
		fs.logger.Warn(err.Error())
		return false
	}

	msg := append([]byte(r.GetClientName()), []byte(r.GetDateTime())...)
	msgHash := sha512.Sum512(msg)

	if err := rsa.VerifyPKCS1v15(clientPubKey.(*rsa.PublicKey), crypto.SHA512, msgHash[:], r.GetClientSignature()); err != nil {
		fs.logger.Warn(err.Error())
		return false
	}

	return true
}

//validateActionRequest checks that a request to read a file is valid. response is always non-nil if result is anything other than ResultValid
func (fs *FileServer) validateActionRequest(action fileServerAction, req *thisrpc.FileActionRequest) (ok bool, response *thisrpc.FileActionResponse) {
	clientName := req.GetClientName()
	fileName := req.GetFileName()

	if !fs.clientIsValid(req) {
		//invalid client
		fs.logger.Warnf("%s request for %s, client %s is rejected (invalid signature or unknown client)", action, fileName, clientName)
		response = &thisrpc.FileActionResponse{
			Error: &thisrpc.ErrorMessage{
				Error:   thisrpc.ErrorMessage_CLIENT_DENIED,
				Message: "invalid signature or unkown client",
			},
		}

	} else if req.GetFileName() == CLIENT_PUBKEY_FILE {
		//Not allowed to read the public key file
		fs.logger.Warnf("%s request for %s, client %s is rejected (writing to public key file is forbidden)", action, fileName, clientName)
		response = &thisrpc.FileActionResponse{
			Error: &thisrpc.ErrorMessage{
				Error:   thisrpc.ErrorMessage_BAD_REQUEST,
				Message: "Reading public key file is forbidden",
			},
		}

	} else if strings.Contains(req.GetFileName(), "/") {
		//invalid chars in file name
		fs.logger.Warnf("%s request for %s, client %s is rejected (invalid chars in file name)", action, fileName, clientName)
		response = &thisrpc.FileActionResponse{
			Error: &thisrpc.ErrorMessage{
				Error:   thisrpc.ErrorMessage_BAD_REQUEST,
				Message: "invalid characters in file name",
			},
		}
	}

	ok = true

	return
}

func errNotFound(fileName string) *thisrpc.FileActionResponse {
	return &thisrpc.FileActionResponse{
		FileName: fileName,
		Error: &thisrpc.ErrorMessage{
			Error:   thisrpc.ErrorMessage_NOT_FOUND,
			Message: "File does not exist or could not be found",
		},
	}
}

func errAlreadyOpen(fileName string) *thisrpc.FileActionResponse {
	return &thisrpc.FileActionResponse{
		FileName: fileName,
		Error: &thisrpc.ErrorMessage{
			Error:   thisrpc.ErrorMessage_ALREADY_OPEN,
			Message: "File is already open",
		},
	}
}

func errNotOpen(fileName string) *thisrpc.FileActionResponse {
	return &thisrpc.FileActionResponse{
		FileName: fileName,
		Error: &thisrpc.ErrorMessage{
			Error:   thisrpc.ErrorMessage_NOT_OPEN,
			Message: "File must be opened before reading or writing",
		},
	}
}

func errEndOfFile(fileName string) *thisrpc.FileActionResponse {
	return &thisrpc.FileActionResponse{
		FileName: fileName,
		Error: &thisrpc.ErrorMessage{
			Error: thisrpc.ErrorMessage_END_OF_FILE,
		},
	}
}

func errStreamError(fileName string, err error) *thisrpc.FileActionResponse {
	return &thisrpc.FileActionResponse{
		FileName: fileName,
		Error: &thisrpc.ErrorMessage{
			Error:   thisrpc.ErrorMessage_STREAM_ERROR,
			Message: err.Error(),
		},
	}
}

func errServerError(filename string, err ...error) *thisrpc.FileActionResponse {
	var msg string
	switch len(err) {
	case 0:
		msg = "internal server error"
	default:
		msg = err[0].Error()
	}

	return &thisrpc.FileActionResponse{
		Error: &thisrpc.ErrorMessage{
			Error:   thisrpc.ErrorMessage_SERVER_ERROR,
			Message: msg,
		},
	}
}

func (fs *FileServer) closeClientIOHandle(client *clientHandle, handleName string) func() {
	return func() {
		client.mtx.Lock()
		defer client.mtx.Unlock()
		h := client.ioHandles[handleName]
		h.mtx.Lock()
		defer h.mtx.Unlock()
		fs.logger.Debugf("Closing file %s for client %s", handleName, client.name)
		h.handle.Close()
		h.timeout.Stop()
		delete(client.ioHandles, handleName)
	}
}
