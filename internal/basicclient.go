package internal

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"time"

	thisrpc "gitlab.com/trbroyles1/rfileserver/rpc"
	"google.golang.org/grpc"
)

const keyFileName = "clientkey.pem"

type BasicClient struct {
	clientName string
	rng        io.Reader
	privateKey *rsa.PrivateKey
	rpcClient  thisrpc.FileStoreServiceClient
	rpcConn    *grpc.ClientConn
}

func NewBasicClient(clientName string) (*BasicClient, error) {
	keyFileBytes, err := ioutil.ReadFile(keyFileName)
	if err != nil {
		return nil, err
	}

	keyFilePem, _ := pem.Decode(keyFileBytes)

	privKey, err := x509.ParsePKCS1PrivateKey(keyFilePem.Bytes)
	if err != nil {
		return nil, err
	}

	return &BasicClient{
		clientName: clientName,
		privateKey: privKey,
		rng:        rand.Reader,
	}, nil
}

func (bc *BasicClient) Connect(addr string) error {
	var opts = []grpc.DialOption{
		grpc.WithInsecure(),
	}

	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		return err
	}
	bc.rpcConn = conn
	bc.rpcClient = thisrpc.NewFileStoreServiceClient(bc.rpcConn)

	return nil
}

func (bc *BasicClient) Close() error {
	return bc.rpcConn.Close()
}

func (bc *BasicClient) CreateFile(fileName string) error {
	req := &thisrpc.CreateFileRequest{
		ReqDetails: &thisrpc.FileActionRequest{
			ClientName: bc.clientName,
			FileName:   fileName,
		},
	}

	if err := bc.signRequest(req.ReqDetails); err != nil {
		return err
	}

	resp, err := bc.rpcClient.CreateFile(context.Background(), req)
	if err != nil {
		return err
	}

	if err := resp.GetRespDetails().GetError(); err != nil {
		return errors.New(err.GetMessage())
	}

	return nil
}

func (bc *BasicClient) OpenFile(fileName string) error {
	req := &thisrpc.OpenFileRequest{
		ReqDetails: &thisrpc.FileActionRequest{
			ClientName: bc.clientName,
			FileName:   fileName,
		},
	}

	if err := bc.signRequest(req.ReqDetails); err != nil {
		return err
	}

	resp, err := bc.rpcClient.OpenFile(context.Background(), req)
	if err != nil {
		return err
	}

	if err := resp.GetRespDetails().GetError(); err != nil {
		if err.GetError() == thisrpc.ErrorMessage_NOT_FOUND {
			return os.ErrNotExist
		} else {
			return errors.New(err.GetMessage())
		}
	}

	return nil
}

func (bc *BasicClient) ReadFile(fileName string, dest []byte) (int, error) {
	req := &thisrpc.FileReadRequest{
		Len: int32(len(dest)),
		ReqDetails: &thisrpc.FileActionRequest{
			ClientName: bc.clientName,
			FileName:   fileName,
		},
	}

	if err := bc.signRequest(req.ReqDetails); err != nil {
		return 0, err
	}

	resp, err := bc.rpcClient.ReadFromFile(context.Background(), req)
	if err != nil {
		return 0, err
	}

	copy(dest, resp.GetData()[:resp.GetLen()])

	if err := resp.GetRespDetails().GetError(); err != nil {
		if err.GetError() == thisrpc.ErrorMessage_NOT_FOUND {
			return int(resp.GetLen()), os.ErrNotExist
		} else if err.GetError() == thisrpc.ErrorMessage_END_OF_FILE {
			return int(resp.GetLen()), io.EOF
		}

		return int(resp.GetLen()), errors.New(err.GetMessage())
	}

	return int(resp.GetLen()), nil
}

func (bc *BasicClient) WriteFile(fileName string, data []byte) (int, error) {
	req := &thisrpc.FileWriteRequest{
		Data: data,
		ReqDetails: &thisrpc.FileActionRequest{
			ClientName: bc.clientName,
			FileName:   fileName,
		},
	}

	if err := bc.signRequest(req.ReqDetails); err != nil {
		return 0, err
	}

	resp, err := bc.rpcClient.WriteToFile(context.Background(), req)
	if err != nil {
		return 0, err
	}

	if err := resp.GetRespDetails().GetError(); err != nil {
		if err.GetError() == thisrpc.ErrorMessage_NOT_FOUND {
			return int(resp.GetLen()), os.ErrNotExist
		}

		return int(resp.GetLen()), errors.New(err.GetMessage())
	}

	return int(resp.GetLen()), nil
}

func (bc *BasicClient) CloseFile(fileName string) error {
	req := &thisrpc.CloseFileRequest{
		ReqDetails: &thisrpc.FileActionRequest{
			ClientName: bc.clientName,
			FileName:   fileName,
		},
	}

	if err := bc.signRequest(req.ReqDetails); err != nil {
		return err
	}

	resp, err := bc.rpcClient.CloseFile(context.Background(), req)
	if err != nil {
		return err
	}

	if err := resp.GetRespDetails().GetError(); err != nil {
		return errors.New(err.GetMessage())
	}

	return nil
}

func (bc *BasicClient) signRequest(req *thisrpc.FileActionRequest) error {
	dateTimeString := time.Now().Format(time.RFC3339Nano)
	unhashedMsg := append([]byte(bc.clientName), []byte(dateTimeString)...)
	hashedMsg := sha512.Sum512(unhashedMsg)
	sig, err := rsa.SignPKCS1v15(bc.rng, bc.privateKey, crypto.SHA512, hashedMsg[:])
	if err != nil {
		return err
	}

	req.DateTime = dateTimeString
	req.ClientSignature = sig

	return nil
}
