package internal

import (
	"net"
	"time"

	logging "gitlab.com/trbroyles1/golog"
)

type connectionLoggingWrapper struct {
	logger      logging.Logger
	wrappedConn net.Conn
}

func newConnectionLoggingWrapper(l logging.Logger, c net.Conn) *connectionLoggingWrapper {
	return &connectionLoggingWrapper{
		logger:      l,
		wrappedConn: c,
	}
}

func (c *connectionLoggingWrapper) Read(b []byte) (int, error) {
	return c.wrappedConn.Read(b)
}

func (c *connectionLoggingWrapper) Write(b []byte) (int, error) {
	return c.wrappedConn.Write(b)
}

func (c *connectionLoggingWrapper) Close() error {
	c.logger.Infof("Closing connection from %s", c.wrappedConn.RemoteAddr().String())
	return c.wrappedConn.Close()
}

func (c *connectionLoggingWrapper) LocalAddr() net.Addr {
	return c.wrappedConn.LocalAddr()
}

func (c *connectionLoggingWrapper) RemoteAddr() net.Addr {
	return c.wrappedConn.RemoteAddr()
}

func (c *connectionLoggingWrapper) SetDeadline(t time.Time) error {
	return c.wrappedConn.SetDeadline(t)
}

func (c *connectionLoggingWrapper) SetReadDeadline(t time.Time) error {
	return c.wrappedConn.SetReadDeadline(t)
}

func (c *connectionLoggingWrapper) SetWriteDeadline(t time.Time) error {
	return c.wrappedConn.SetWriteDeadline(t)
}

type ListenerLoggingWrapper struct {
	logger          logging.Logger
	wrappedEndpoint net.Listener
}

func NewConnectionLoggingListener(l logging.Logger, e net.Listener) *ListenerLoggingWrapper {
	return &ListenerLoggingWrapper{
		logger:          l,
		wrappedEndpoint: e,
	}
}

func (l *ListenerLoggingWrapper) Accept() (net.Conn, error) {
	c, err := l.wrappedEndpoint.Accept()
	if err != nil {
		l.logger.Warn(err.Error())
	} else {
		l.logger.Infof("Accepted new connection from %s", c.RemoteAddr().String())

	}
	return newConnectionLoggingWrapper(l.logger, c), err
}

func (l *ListenerLoggingWrapper) Close() error {
	return l.wrappedEndpoint.Close()
}

func (l *ListenerLoggingWrapper) Addr() net.Addr {
	return l.wrappedEndpoint.Addr()
}
