# rFileServer

rFileServer is a tool that provides an extremely simple file server and client over gRPC. 
- Each client gets a bucket of space in which to store files. That's it. No folders, no handling of permissions, just a bucket of space
- Clients are not able to see, create, modify or delete files for each other. In other words, a client's bucket is private to that client only
- Clients sign all requests to the server with an RSA private key. The server validates the client's signature with the corresponding public key before handling the client's request

the `ioclient` package provides an extremely simple File-like abstraction on top of the basic client, which is defined in the `internal` package.

The server chroots itself into the base directory of its file repository as a mandatory step of it's initialization. This is to prevent any possibility of the client passing a malicious request that could allow it to escape the file repository. We try to guard against this in the code also (namely by not allowing `/` in file names); the chrooting is an extra layer of security.

## Building the project
1. Clone the repo
2. run `go build`

Or, to use the build script (requires [Mage](https://magefile.org/) and [govvv](https://github.com/ahmetb/govvv)):
1. Clone the repo
2. run `mage build`

## Running the server

**rFileServer must be started as root. This is so it can chroot itself to the base directory of its file repository.** Root permissions are dropped as soon as this step is completed.

Two cmdline flags must be passed when starting the server:
- `--base-directory` - the base file repository directory. rFileServer will chroot itself to this directory.
- `--setuid-user` - rFileServer will change to this user ID as soon as it successfully chroots itself.

Other flags are also available; see the output of `rfileserver help run` for more details.

It is suggested to create a dedicated user/group for rFileServer, and to make it the owner of the base repo directory.

Given a base directory of `/data/rfileserver/repo` and a dedicated `rfileserver` user with uid `1008`, the command to run the server is:

```
rfileserver run --base-directory /data/rfileserver/repo --setuid-user 1008
```

By default, the server will bind to `:5678`.

## Using the client

**The client currently expects to find the private key used to sign requests to the server in a file called `clientkey.pem`, located in working directory of the application using the client.** If this condition is not satisfied, it will fail to connect. See the File Repository and Keys section below for information on generating and using this file.

Call `ioclient.Connect`, passing it a `clientName` and a `serverAddr`
- `clientName` is the name the client should use to talk to the server
- `serverAddr` is the host:port of the server

If `Connect` is unable to connect, the returned `err` will explain why. In this case, the returned `*ioClient` will be `nil`.

Given a client name of `testClient` and a server address of `10.10.10.3:5678` the complete call would be:

```
client, err := ioClient.Connect("testClient", "10.10.10.3:5678")
if err != nil {
    //handle the connection error
}

//use the client to read / write files
```

Once connected, the returned `*ioClient` will have `os`-like `Create` and `Open` methods, each of which will return an `io.ReadWriteCloser`.

Note that **it is an error to use the `*ioClient` to open a file more than once concurrently**. In other words, you will get an error if you call `Create` or `Open` for a given file name, then try to call `Create` or `Open` again with that same file name without first calling `Close` on the `io.ReadWriteCloser` returned by the first call to `Create` or `Open`.

Be sure to call `Close` on the `*ioClient` once you are finished reading / writing files.

## File Repository and Keys

### Repository structure

A dedicated directory needs to be established for the server to use as its file repository.
This directory must contain one subdirectory for each client.
There must exist a file called `clientkey.pub.pem` inside the subdirectory, containing the PEM-encoded RSA public key corresponding to the client's private key.

New subdirectories and keyfiles must be installed manually for new clients; the server will not do it for you.

Given a base directory of `/rfileserver/repo` and two clients, `client-one` and `client-two`, the proper directory structure is:
```
/rfileserver
    /repo
        /client-one
            -> clientkey.pub.pem
        /client-two
            -> clientkey.pub.pem
```
Files created by clients will exist in their subdirectory, alongside their `clientkey.pub.pem`. Clients will not be able to see or access `clientkey.pub.pem`

### Generating and using a new key pair
1. Follow the instructions in the file `GENERATING A NEW CLIENT KEY PAIR` in this repo. This will give you two files: `clientkey.pem` containing the private key for the client, and `clientkey.pub.pem` containing the corresponding public key to be placed on the server
2. Place `clientkey.pub.pem` into the client's repo directory, as explained above in Repository Structure
3. Place `clientkey.pem` into the directory of the application using the client.
